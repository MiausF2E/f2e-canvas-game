import Vue from 'vue'
import Vuex from 'vuex'

import { eventBus } from './game/helpers'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentScreen: 'title',
    currentStage: 1,
    currentSkill: '',
    battery: 0,
    heart: 3,
    stamina: 0
  },
  mutations: {
    ChangeStage (state, stage) {
      state.currentStage = stage
    },
    StartGame (state) {
      state.currentScreen = 'game'
      state.heart = 3
      state.stamina = 0
      eventBus.emit('game-start')
    },
    BackToTitle (state) {
      state.currentScreen = 'title'
      eventBus.emit('back-to-title')
    },
    activateSkill (state, { skill, timeout }) {
      state.currentSkillTimeout = timeout
      state.currentSkill = skill
    },
    deactivateSkill (state) {
      state.currentSkill = ''
    }
  },
  actions: {
    UpdateHeart ({ state }, heart) {
      state.heart = heart
    }
  }
})
