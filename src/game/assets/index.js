import { app } from '@/game/helpers'

app.loader
  .reset()
  .add('skill-shield', require('./icons/icons-04.png'))
  .add('skill-double', require('./icons/icons-05.png'))
  .add('item-heart', require('./icons/icons-06.png'))
  .add('item-nuke', require('./icons/icons-07.png'))
  .add('skill-wave', require('./icons/icons-08.png'))
  .add('skill-laser', require('./icons/icons-09.png'))
  .load(() => console.log('resource loaded:', app.loader.resources))
