import { Application } from 'pixi.js'
import { init, eventBus } from './helpers'

export default function () {
  const app = new Application({
    view: this.$el,
    antialias: true,
    resolution: window.devicePixelRatio,
    transparent: true
  })

  function startPixi () {
    init(this, app)
    require('./assets')
    require('./bootstrap')
  }

  if (module.hot) {
    module.hot.accept([
      './assets',
      './bootstrap',
      './stage'
    ], startPixi)
  }

  startPixi()
  eventBus.on('game-start', () => require('./stage'))
}
