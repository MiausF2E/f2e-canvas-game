export class Vector2D {
  constructor (x = 0, y = 0) {
    this.set(x, y)
  }
  set (x, y) {
    this.x = x
    this.y = y
  }
  move (x, y) {
    this.x += x
    this.y += y
  }
  add (v) {
    return new Vector2D(this.x + v.x, this.y + v.y)
  }
  sub (v) {
    return new Vector2D(this.x - v.x, this.y - v.y)
  }
  mul (m) {
    return new Vector2D(this.x * m, this.y * m)
  }
  get length () {
    return Math.sqrt(this.x * this.x + this.y * this.y)
  }
  set length (newV) {
    let temp = this.unit.mul(newV)
    this.set(temp.x, temp.y)
  }
  clone () {
    return new Vector2D(this.x, this.y)
  }
  toString () {
    return `(${this.x}, ${this.y})`
  }
  equal (v) {
    return this.x === v.x && this.y === v.y
  }
  get angle () {
    return Math.atan2(this.y, this.x)
  }
  get unit () {
    return this.mul(1 / this.length)
  }
}
