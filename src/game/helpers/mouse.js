import { app } from './constants'

export function mouse (targetElement = app.view) {
  function noop () {}
  let mouse = {
    down: noop,
    up: noop,
    move: noop,
    downHandler: evt => mouse.down(evt),
    upHandler: evt => mouse.up(evt),
    moveHandler: evt => mouse.move(evt)
  }
  targetElement.addEventListener('mousedown', mouse.downHandler.bind(mouse), false)
  targetElement.addEventListener('mouseup', mouse.upHandler.bind(mouse), false)
  targetElement.addEventListener('mousemove', mouse.moveHandler.bind(mouse), false)
  return mouse
}
