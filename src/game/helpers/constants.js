import EE3 from 'eventemitter3'

/**
 * import and save instance
 */
export let vue, app
export function init (vueInstance, appInstance) {
  vue = vueInstance
  app = appInstance
}

/**
 * eventBus
 */
export const eventBus = new EE3()

/**
 * read debug from localStorage
 */
export const isDebug = localStorage.isDebug === 'true'

export const degToPi = Math.PI / 180
