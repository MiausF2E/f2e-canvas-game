export function keyboard (keyCode) {
  function noop () {}

  const key = {
    code: keyCode,
    isDown: false,
    isUp: true,
    press: noop,
    release: noop,
    downHandler: event => {
      event.preventDefault()
      if (event.keyCode === key.code) {
        if (key.isUp && key.press) key.press()
        key.isDown = true
        key.isUp = false
      }
    },
    upHandler: event => {
      event.preventDefault()
      if (event.keyCode === key.code) {
        if (key.isDown && key.release) key.release()
        key.isDown = false
        key.isUp = true
      }
    }
  }
  // Attach event listeners
  window.addEventListener('keydown', key.downHandler.bind(key), false)
  window.addEventListener('keyup', key.upHandler.bind(key), false)

  return key
}
