import {
  Container,
  Graphics
} from 'pixi.js'

import { degToPi } from '@/game/helpers'

const bunker = new Container()
const bunkerLength = 30
bunker.setTransform(bunkerLength / 2, bunkerLength / 2)

const innerCircle = new Graphics()
innerCircle
  .lineStyle(5, 0xffffff, 1)
  .drawCircle(0, 0, 30)
bunker.addChild(innerCircle)

const line1 = new Graphics()
line1
  .lineStyle(2, 0xffffff, 1)
  .moveTo(0, 0)
  .lineTo(0, -30)
bunker.addChild(line1)

const line2 = new Graphics()
const line2Angle = -90 * degToPi - (2 * Math.PI / 3)
line2
  .lineStyle(2, 0xffffff, 1)
  .moveTo(0, 0)
  .lineTo(Math.cos(line2Angle) * 30, Math.sin(line2Angle) * 30)
bunker.addChild(line2)

const line3 = new Graphics()
const line3Angle = -90 * degToPi + (2 * Math.PI / 3)
line3
  .lineStyle(2, 0xffffff, 1)
  .moveTo(0, 0)
  .lineTo(Math.cos(line3Angle) * 30, Math.sin(line3Angle) * 30)
bunker.addChild(line3)

const dotLength = 55
Array.from({length: 45}, (value, index) => {
  const angle = index * (Math.PI * 2 / 45) * (parseInt(index / 180) ? -1 : 1)
  const dot = new Graphics()
  dot
    .beginFill(0xffffff)
    .drawCircle(dotLength * Math.cos(angle), dotLength * Math.sin(angle), 1)
    .endFill()
  bunker.addChild(dot)
})

const shield = new Graphics()
shield
  .lineStyle(5, 0xffffff, 1)
  .arc(0, 0, 65, 45 * degToPi, 135 * degToPi)
bunker.addChild(shield)

const ship = new Graphics()
ship
  .beginFill(0xffffff)
  .drawPolygon([
    10, 0,
    10, 15,
    -10, 15,
    -10, 0,
    -5, -15,
    5, -15
  ])
  .endFill()
ship.y = -55
bunker.addChild(ship)

export {
  bunker
}
