import { app, eventBus, mouse, degToPi, Vector2D } from '@/game/helpers'
import { bunker } from './vectors'

const pointer = mouse()
const mousePos = new Vector2D()
eventBus.on('resize', (width, height) => {
  pointer.move = ({x, y}) => {
    mousePos.set(x - width / 2, y - height / 2)
    bunker.rotation = mousePos.angle - -90 * degToPi
  }
})
window.dispatchEvent(new Event('resize'))
bunker.x = 0
bunker.y = 0
app.stage.addChild(bunker)

/*
import { Vector2D, degToPi } from '@/game/helpers'

const a = new Vector2D(3, 4)

console.log(a.length, a.angle, a.angle / degToPi, Math.sin(a.angle) * a.length, Math.cos(a.angle) * a.length)
*/
