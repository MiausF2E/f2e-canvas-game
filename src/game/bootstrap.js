import {
  app,
  degToPi,
  eventBus,
  isDebug,
  mouse,
  Vector2D
} from './helpers'

import {
  Container,
  Graphics,
  Text,
  TextStyle
} from 'pixi.js'

app.renderer.autoResize = true

function updateSize () {
  app.renderer.resize(window.innerWidth, window.innerHeight)
  const { width, height } = app.view.getBoundingClientRect()
  eventBus.emit('resize', width, height)
}
window.addEventListener('resize', updateSize)

/* setup stage */
const { stage } = app
eventBus.on('resize', (width, height) => {
  stage.setTransform(width / 2, height / 2)
})

if (isDebug || (isDebug !== false && process.env.NODE_ENV === 'development')) {
  window.app = app

  /* coordinate helper */
  const lineX = new Graphics()
  lineX.lineStyle(1, 0xffffff, 1)
  stage.addChild(lineX)
  eventBus.on('resize', (width, height) => {
    const half = width / 2
    lineX.moveTo(-half, 0)
    lineX.lineTo(half, 0)
  })

  const lineY = new Graphics()
  lineY.lineStyle(1, 0xffffff, 1)
  stage.addChild(lineY)
  eventBus.on('resize', (width, height) => {
    const half = height / 2
    lineY.moveTo(0, -half)
    lineY.lineTo(0, half)
  })

  /* pointer helper */
  const length = 20

  const pointerGroup = new Container()
  pointerGroup.setTransform(length / 2, length / 2)

  const pointerX = new Graphics()
  pointerGroup.addChild(pointerX)
  pointerX.lineStyle(1, 0xff0000, 1)
  pointerX.moveTo(-length, 0)
  pointerX.lineTo(length, 0)

  const pointerY = new Graphics()
  pointerY.lineStyle(1, 0xff0000, 1)
  pointerY.moveTo(0, -length)
  pointerY.lineTo(0, length)
  pointerGroup.addChild(pointerY)

  const pointerDot = new Graphics()
  pointerDot.beginFill(0xff0000)
  pointerDot.drawCircle(0, 0, 5)
  pointerDot.endFill()
  pointerGroup.addChild(pointerDot)

  const textStyle = new TextStyle({
    fontSize: 12,
    fill: 'white'
  })
  const pointerText = new Text('', textStyle)
  pointerText.x = 10
  pointerText.y = 10
  pointerGroup.addChild(pointerText)

  const pointerCircle = new Graphics()
  stage.addChild(pointerCircle)

  /* show angel text */
  const angleText = new Text('', textStyle)
  angleText.x = 10
  angleText.y = 10
  stage.addChild(angleText)

  /* show circle radius */
  const radiusText = new Text('', textStyle)
  angleText.y = 10
  stage.addChild(radiusText)

  /* show fps */
  const fpsIndicator = new Text(`FPS: ${app.ticker.FPS.toFixed(5)}`, {
    fontSize: 12,
    fill: 'green'
  })
  stage.addChild(fpsIndicator)
  eventBus.on('resize', (width, height) => {
    fpsIndicator.x = width / 2 - 100
    fpsIndicator.y = -height / 2
  })
  app.ticker.add(() => {
    fpsIndicator.text = `FPS: ${app.ticker.FPS.toFixed(5)}`
  })

  pointerGroup.x = 0
  pointerGroup.y = 0
  stage.addChild(pointerGroup)

  const mousePos = new Vector2D()
  const pointerHandler = mouse()

  eventBus.on('resize', (width, height) => {
    pointerHandler.move = ({x, y}) => {
      mousePos.set(x - width / 2, y - height / 2)
      const { length } = mousePos
      pointerText.text = mousePos.toString()
      angleText.text = parseInt(mousePos.angle / degToPi) + '度'
      pointerCircle
        .clear()
        .lineStyle(1, 0xff0000, 1)
        .drawCircle(0, 0, length)
      pointerGroup.x = mousePos.x
      pointerGroup.y = mousePos.y
      radiusText.x = length + 10
      radiusText.text = `r = ${length}`
    }
  })
}

/* initial stage */
updateSize()
