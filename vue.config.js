const { name } = require('./package.json')

module.exports = {
  baseUrl: `/${name}/`
}
